oc set volume dc/rhpam77-kieserver --add \
--name=jar --configmap-name=custom-jar --default-mode=0664 \
--mount-path=/opt/eap/standalone/deployments/ROOT.war/WEB-INF/lib/core-gql-event-listener-0.0.1-SNAPSHOT.jar \
--sub-path=core-gql-event-listener-0.0.1-SNAPSHOT.jar

oc create configmap custom-jar --from-file=core-gql-event-listener-0.0.1-SNAPSHOT.jar
