package com.anseriform.jbpm;

import org.jboss.logging.Logger;


import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jbpm.services.task.events.DefaultTaskEventListener;
import org.kie.api.task.TaskEvent;

import javax.ws.rs.core.UriBuilder;

/*
 * Custom TaskEventListener implementation for demo purposes
 *
 * Note that the listener does not directly implement
 * org.jbpm.services.task.lifecycle.listeners.TaskLifeCycleEventListener.
 * It extends DefaultTaskEventListener instead, so that we only have to implement
 * the methods we are interested in.
 *
 * https://github.com/droolsjbpm/jbpm/blob/master/jbpm-human-task/jbpm-human-task-core/src/main/java/org/jbpm/services/task/events/DefaultTaskEventListener.java
 *
 */
public class AnseriformTaskEventListener extends DefaultTaskEventListener {

	private static final Logger LOGGER = Logger.getLogger(AnseriformTaskEventListener.class.getName());
	private String anseriformHost = System.getProperty("com.anseriform.core-gql.host", "http://core-gql-go");

	@Override
	public void beforeTaskStartedEvent(TaskEvent event) {
		LOGGER.info("Starting task " + event.getTask().getId());
	}

	@Override
	public void afterTaskCompletedEvent(TaskEvent event) {
		LOGGER.info("Completed task " + event.getTask().getId());
	}

	@Override
	public void afterTaskSkippedEvent(TaskEvent event) {
		LOGGER.info("Skipped task " + event.getTask().getId());
	}
}
