Project to demonstrate the creation of a custom TaskEventListener implementation

Note that the listener does not directly implement
org.jbpm.services.task.lifecycle.listeners.TaskLifeCycleEventListener.
It extends DefaultTaskEventListener instead, so that we only have to implement
the methods we are interested in.

https://github.com/droolsjbpm/jbpm/blob/master/jbpm-human-task/jbpm-human-task-core/src/main/java/org/jbpm/services/task/events/DefaultTaskEventListener.java

Installation:
------------

1. Build the project by executing:

$ mvn clean package

2. Deploy the artifact to business-central.war

cp target/custom-task-event-listener-0.0.1-SNAPSHOT.jar $BPMS_HOME/jboss-eap-6.1/standalone/deployments/business-central.war/WEB-INF/lib/
