package com.anseriform.jbpm;

import org.jboss.logging.Logger;
import org.kie.api.event.process.*;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

//com.anseriform.jbpm.AnseriformProcessEventListener
public class AnseriformProcessEventListener implements ProcessEventListener {
	private static final Logger LOGGER = Logger.getLogger(AnseriformProcessEventListener.class.getName());
	private String anseriformHost;

	HttpClient client;

	private void sendUpdate(String instanceId) {
		String upUrl = anseriformHost + "/webhook?instanceId=" + instanceId;
		LOGGER.info("Sending Request " + upUrl);

		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(upUrl))
				.build();

		client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
				.thenApply(HttpResponse::body)
				.thenAccept(System.out::println)
				.join();
	}

	public AnseriformProcessEventListener() {
		anseriformHost = System.getProperty("com.anseriform.core-gql.host", "http://core-gql-go");
		LOGGER.info("Starting Process Event Listener wth host " + anseriformHost);
		client = HttpClient.newHttpClient();
	}

	public void beforeProcessStarted(ProcessStartedEvent event) {
		LOGGER.info("Starting process " + event.getProcessInstance().getProcessName() + event.getProcessInstance().getId());
//		proxy.postProcessEvent("" + event.getProcessInstance().getId());
	}

	public void afterProcessStarted(ProcessStartedEvent event) {
		LOGGER.info("Started process " + event.getProcessInstance().getId());
		sendUpdate("" + event.getProcessInstance().getId());
	}

	public void beforeProcessCompleted(ProcessCompletedEvent event) {
		LOGGER.info("Completing process " + event.getProcessInstance().getId());
//		proxy.postProcessEvent("" + event.getProcessInstance().getId());
	}

	public void afterProcessCompleted(ProcessCompletedEvent event) {
		LOGGER.info("Completed process " + event.getProcessInstance().getId());
		sendUpdate("" + event.getProcessInstance().getId());
	}

	public void beforeNodeTriggered(ProcessNodeTriggeredEvent event) {
//		LOGGER.info("Triggering Node process " + event.getProcessInstance().getId());
//		proxy.postProcessEvent("" + event.getProcessInstance().getId());
	}

	public void afterNodeTriggered(ProcessNodeTriggeredEvent event) {
		LOGGER.info("Triggered Node process " + event.getNodeInstance().getNodeName());
//		proxy.postProcessEvent("" + event.getProcessInstance().getId());
	}

	public void beforeNodeLeft(ProcessNodeLeftEvent event) {
		LOGGER.info("Completing Node process " + event.getNodeInstance().getNodeName());
//		proxy.postProcessEvent("" + event.getProcessInstance().getId());
	}

	public void afterNodeLeft(ProcessNodeLeftEvent event) {
		LOGGER.info("Completed Node process " + event.getNodeInstance().getNodeName());
		sendUpdate("" + event.getProcessInstance().getId());
	}

	public void beforeVariableChanged(ProcessVariableChangedEvent event) {
//		LOGGER.info("Changing Variable process " + event.getProcessInstance().getId() + " variable " + event.getVariableInstanceId());
//		proxy.postProcessEvent("" + event.getProcessInstance().getId());
	}

	public void afterVariableChanged(ProcessVariableChangedEvent event) {
//		LOGGER.info("Changed Variable process " + event.getProcessInstance().getId());
//		sendUpdate("" + event.getProcessInstance().getId());
	}
}
